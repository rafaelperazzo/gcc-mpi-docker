FROM gcc:11.2.0
WORKDIR /usr/src/app
RUN apt-get update
RUN apt-get -y install wget
RUN wget -O openmpi.tar.bz2 "https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.1.tar.bz2" && \
    tar jxvf openmpi.tar.bz2
RUN /usr/src/app/openmpi-4.1.1/./configure --prefix=/usr/local --with-hwloc=internal --with-libevent=internal
RUN cd /usr/src/app/openmpi-4.1.1/
RUN make all install
RUN ldconfig
RUN useradd -ms /bin/bash mpi
USER mpi    
